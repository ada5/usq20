# Naval Tactical Data Systems (NTDS) Emulator

Create a system that emulates the full NTDS suite of hardware as used at it's inception
around 1962.

## Project goal:

Emulate the following equipment:

*Initially:*

    * Computer
        * CP642B/Univac 1230 Computer

    * External Memory
        * Control Data Corp (CDC) MU-602 External Core Module Unit (ECMU)

*Future:*

    * Computer
        * CP-642A/Univac 1206 Computer
        * CP-642B/Univac 1212 Computer

    * Magnetic Tape Unit
        * RD-243 ? RD-281
        * RD-358

    * Hardcopy and Paper Tape Unit
        * AN/UGC-13 Paper Tape Punch-Reader/Teletypewriter (TTY)

*Possible future:*

    * Univac ASR-28 Paper Tape Punch-Reader/Teletypewriter (TTY)

    * Link-11 Terminal

    * Beacon Video Processor (BVP)

    * SYA-4 Display System

    * UYA-4 Display System

    * CP-1218/9 Computer (used by KCMX and BVP)

## Considerations

    * Execute instructions and interfaces using the original equipment specifications:

        * CP642 executes program instructions at 1MHz divided into 4 clock phases.
          Emulator will execute instructions at same rate.

        * Try to emulate hardware as close as possible

## Caveats

    * Simulate hardware wiring between equipment utilizing network packets.
      Be prepared for network storm as individual packets will be control bits and data bits as one packet.

