# USQ-20/CP642A Repertoire Of Instructions

This document describes the instruction set that the USQ-20/CP642A computer will execute.

Unless otherwise specified, all numbers are octal base.

## 1's Complement Example

* (Y)<sup>!</sup> (Pronounce "Y prime") means 1's complement (bit-for-bit inversion) of the contents of (Y).

Example 1

|                | Bits            |
| -------------- | --------------- |
| Y<sub>i</sub>  | 1 1 1 1 1 1 1 1 |
| Y<sub>f</sub>  | 0 0 0 0 0 0 0 0 |

Example 2

|                | Bits            |
| -------------- | --------------- |
| Y<sub>i</sub>  | 1 0 1 0 1 0 1 0 |
| Y<sub>f</sub>  | 0 1 0 1 0 1 0 1 |


## Logical Product Example

* L[Y1*Y2] indicates Logical Product. Logical product means:
    * L[Y1*Y2]<sup>nn</sup> == 1 when bit (Y1)<sup>nn</sup> == 1 AND (Y2)<sup>nn</sup> == 1
    * L[Y1*Y2]<sup>nn</sup> == 0 when bit (Y1)<sup>nn</sup> == 0 OR  (Y2)<sup>nn</sup> == 0

Example 1

|          | Bits            |
| -------- | --------------- |
| Y1       | 1 1 1 1 1 1 1 1 |
| Y2       | 1 1 1 1 1 1 1 1 |
| L[Y1*Y2] | 1 1 1 1 1 1 1 1 |

Example 2

|          | Bits            |
| -------- | --------------- |
| Y1       | 1 0 1 0 1 0 1 0 |
| Y2       | 1 1 1 1 1 1 1 1 |
| L[Y1*Y2] | 1 0 1 0 1 0 1 0 |

Example 3

|          | Bits            |
| -------- | --------------- |
| Y1       | 1 1 1 1 1 1 1 1 |
| Y2       | 0 1 0 1 0 1 0 1 |
| L[Y1*Y2] | 0 1 0 1 0 1 0 1 |

Example 4

|          | Bits            |
| -------- | --------------- |
| Y1       | 1 1 1 1 1 1 1 1 |
| Y2       | 0 0 0 0 0 0 0 0 |
| L[Y1*Y2] | 0 0 0 0 0 0 0 0 |

## J/J^ Field Defines

Defines the **j** field modifiers in the instruction.

For I/O instructions, **j^** field specifies channel number and has no J table entry.

### J Table 1 - Skip NI (Normal)

| j | Meaning    | Description                                     |
| - | ---------- | ----------------------------------------------- |
| 0 | No skip    | Do not skip NI                                  |
| 1 | Skip       | Always skip next instruction                    |
| 2 | Q POS      | Jump if Q is positive (bit 2<sup>29</sup> == 0) |
| 3 | Q NEG      | Jump if Q is negative (bit 2<sup>29</sup> == 1) |
| 4 | A ZERO     | Skip NI if A == 0                               |
| 5 | A NOT ZERO | Skip NI if A /= 0                               |
| 6 | A POS      | Jump if A is positive (bit 2<sup>29</sup> == 0) |
| 7 | A NEG      | Jump if A is negative (bit 2<sup>29</sup> == 1) |

### J Table 2 - Skip NI (Add/Sub Q)

| j | Meaning    | Description                                     |
| - | ---------- | ----------------------------------------------- |
| 0 | No skip    | Do not skip NI                                  |
| 1 | Skip       | Always skip next instruction                    |
| 2 | A POS      | Jump if A is positive (bit 2<sup>29</sup> == 0) |
| 3 | A NEG      | Jump if A is negative (bit 2<sup>29</sup> == 1) |
| 4 | Q ZERO     | Skip NI if Q == 0                               |
| 5 | Q NOT ZERO | Skip NI if Q /= 0                               |
| 6 | Q POS      | Jump if Q is positive (bit 2<sup>29</sup> == 0) |
| 7 | Q NEG      | Jump if Q is negative (bit 2<sup>29</sup> == 1) |

### J Table 3 - Skip NI (Compare)

| j | Meaning    | Description                               |
| - | ---------- | ----------------------------------------- |
| 0 | No skip    | Do not skip NI                            |
| 1 | Skip       | Always skip NI                            |
| 2 | Y LESS Q   | Skip NI if Y <= (Q)                       |
| 3 | Y MORE Q   | Skip NI if Y > (Q)                        |
| 4 | Y IN QA    | (Q) >= Y > (A)                            |
| 5 | Y OUT QA   | (Q) < Y <= (A)                            |
| 6 | Y LESS A   | Y <= (A)                                  |
| 7 | Y MORE A   | Y > (A)                                   |

### J Table 4 - Skip NI (Divide)

| j | Meaning    | Description                                     |
| - | ---------- | ----------------------------------------------- |
| 0 | No skip    | Do not skip NI                                  |
| 1 | Skip       | Alays skip NI                                   |
| 2 | NO OVF     | Skip NI if there is no overflow                 |
| 3 | OVF        | Skip NI if there is overflow                    |
| 4 | A ZERO     | Skip NI if A == 0                               |
| 5 | A NOT ZERO | Skip NI if A /= 0                               |
| 6 | A POS      | Jump if A is positive (bit 2<sup>29</sup> == 0) |
| 7 | A NEG      | Jump if A is negative (bit 2<sup>29</sup> == 1) |

### J Table 5 - Skip NI (Logical Product)

| j | Meaning    | Description                                     |
| - | ---------- | ----------------------------------------------- |
| 0 | No skip    | Do not skip NI                                  |
| 1 | Skip       | Always skip next instruction                    |
| 2 | EVEN       | Skip if parity is even                          |
| 3 | ODD        | Skip if parity is odd                           |
| 4 | A ZERO     | Skip if A == 0                                  |
| 5 | A NOT ZERO | Skip if A /= 0                                  |
| 6 | A POS      | Jump if A is positive (bit 2<sup>29</sup> == 0) |
| 7 | A NEG      | Jump if A is negative (bit 2<sup>29</sup> == 1) |

### J Table 6 - Repeat

| j | Meaning    | Description                                          |
| - | ---------- | ---------------------------------------------------- |
| 0 | No mod     | Y of NE == Y                                         |
| 1 | ADV        | Y of NE == Y+1                                       |
| 2 | BACK       | Y of NE == Y-1                                       |
| 3 | ADD B      | Y of NE == Y+B<sup>b</sup>                           |
| 4 | RI         | Y of NE == Y (read)                                  |
|   |            | Y of NE == Y+B<sup>6</sup> (store)                   |
| 5 | ADV R      | Y of NE == Y+1 (read)                                |
|   |            | Y of NE == (Y+1)+B<sup>6</sup> (store)               |
| 6 | BACK R     | Y of NE == Y-1 (read)                                |
|   |            | Y of NE == Y-1+B<sup>6</sup> (store)                 |
| 7 | ADD B R    | Y of NE == Y+B<sup>b</sup> (read)                    |
|   |            | Y of NE == Y+B<sup>b</sup>+B<sup>6</sup> (store)     |

Notes:

* (sic) B<sup>6</sup> increment if NI is replace class, increments Y address for store portion of replace
    * Read Y
    * Store Y + B<sup>6</sup>
    * Increment/Decrement Y
* R = Replace class instruction extra modification
* NE = Next execution of repeated instruction
* B<sup>b</sup> indicates B register specified by **b** field of instruction
* B<sup>6</sup> indicates B6 register

### J Table 7 - Jump (Arithmetic)

| j | Meaning    | Description                                     |
| - | ---------- | ----------------------------------------------- |
| 0 | No jump    | Do not jump. Set Interrupt Enable.              |
| 1 | Jump       | Always jump to Y. Set Interrupt Enable.         |
| 2 | Q POS      | Jump if Q is positive (bit 2<sup>29</sup> == 0) |
| 3 | Q NEG      | Jump if Q is negative (bit 2<sup>29</sup> == 1) |
| 4 | A ZERO     | Jump if A == 0                                  |
| 5 | A NOT ZERO | Jump if A /= 0                                  |
| 6 | A POS      | Jump if A is positive (bit 2<sup>29</sup> == 0) |
| 7 | A NEG      | Jump if A is negative (bit 2<sup>29</sup> == 1) |

### J Table 8 - Jump (Select)

| j | Meaning    | Description                                   |
| - | ---------- | --------------------------------------------- |
| 0 | Jump       | Always jump to Y                              |
| 1 | KEY 1      | Jump if key 1 is selected                     |
| 2 | KEY 2      | Jump if key 2 is selected                     |
| 3 | KEY 3      | Jump if key 3 is selected                     |
| 4 | STOP       | Stop. Jump to Y on restart.                   |
| 5 | STOP 5     | Stop if key 5 selected. Jump to Y on restart. |
| 6 | STOP 6     | Stop if key 6 selected. Jump to Y on restart. |
| 7 | STOP 7     | Stop if key 7 selected. Jump to Y on restart. |


## K/K^ Field Defines

Defines the **k** or **k^** field modifiers of instruction (designates where Y is located)

Notes:

* A, Q = A and Q registers
* K = Normal instruction **k** field
* K^ = I/O instruction **k^** field
* CP = 1's Complement (bit level)
* L = Half word - Lower half word (bits 2<sup>14</sup> .. 2<sup>00</sup>)
* M = Full word from/to memory
* U = Half word - Upper half word (bits 2<sup>29</sup> .. 2<sup>15</sup>)
* W = Full Word
* X = Sign extended (bit 2<sup>14</sup> extended through bits 2<sup>29</sup>)
* (Base) = Base address of routine/IO group/etc. IO group being input group, output group, etc.
    * Example: CP642A input buffer address assignments
        * (Base) address for input group is 00100
        * Input buffer for channel 00 address is (Base) + Channel number 0 (00100)
        * Input buffer for channel 01 address is (Base) + channel number 1 (00101)

### K Table 1 - Normal Read

| k | Code    | Origin         |
| - | ------- | -------------- |
| 0 | (blank) | U<sub>L</sub>  |
| 1 | L       | M<sub>L</sub>  |
| 2 | U       | M<sub>U</sub>  |
| 3 | W       | M              |
| 4 | X       | XU<sub>L</sub> |
| 5 | LX      | XM<sub>L</sub> |
| 6 | UX      | XM<sub>U</sub> |
| 7 | A       | A              |

### K Table 2 - Normal Store

| k | Code  | Destination              |
| - | ----- | ------------------------ |
| 0 | Q     | Q                        |
| 1 | L     | M<sub>L</sub>            |
| 2 | U     | M<sub>U</sub>            |
| 3 | W     | M                        |
| 4 | A     | A                        |
| 5 | CPL   | Complement M<sub>L</sub> |
| 6 | CPU   | Complement M<sub>U</sub> |
| 7 | CPW   | Complement M             |

### K Table 3 - Replace

| k | Code  | Origin         | Destination   |
| - | ----- | -------------- | ------------- |
| 0 | (n/a) |                |               |
| 1 | L     | M<sub>L</sub>  |               |
| 2 | U     | M<sub>U</sub>  |               |
| 3 | W     | M              |               |
| 4 | (n/a) |                |               |
| 5 | LX    | XM<sub>L</sub> | M<sub>l</sub> |
| 6 | UX    | XM<sub>U</sub> | M<sub>u</sub> |
| 7 | (n/a) |                |               |

### K Table 4 - **k^** field defines

| k^ | Source          | Destination             | Note                                     |
| -- | --------------- | ----------------------- | ---------------------------------------- |
| 0  | Y               | ((Base)+j^)<sub>L</sub> | ((Base)+j^)<sub>U</sub> remains the same |
| 1  | (Y)<sub>L</sub> | ((Base)+j^)<sub>L</sub> | ((Base)+j^)<sub>U</sub> remains the same |
| 2  | (Y)<sub>U</sub> | ((Base)+j^)<sub>L</sub> | ((Base)+j^)<sub>U</sub> remains the same |
| 3  | (Y)             | (Base)+j^               |                                          |

Buffer layout:

* ((Base)+C<sup>j^</sup>)<sub>U</sub> == Last address of buffer.
* ((Base)+C<sup>j^</sup>)<sub>L</sub> == First/Current address of buffer.

Buffer sequence:

* ((Base)+C<sup>j^</sup>)<sub>U</sub> == ((Base)+C<sup>j^</sup>)<sub>L</sub>: Terminate buffer.
* ((Base)+C<sup>j^</sup>)<sub>U</sub> /= ((Base)+C<sup>j^</sup>)<sub>L</sub>: ((Base)+C<sup>j^</sup>)<sub>L</sub> + 1 -> ((Base)+C<sup>j^</sup>)<sub>L</sub>. Continue I/O operation.

## Repertoire of Instructions (Basic)

Notes:

* OP field indicates instruction
* Code field indicates assembly-style mnemonic for OP field
* j field indicates which J table to use
* k field indicates which K table to use
* Description field indicates expanded instruction meaning and notes
* L = LP = Logical Product (bit-for-bit AND)
* For shift instructions, only bit 2<sup>05</sup> .. 2<sup>00</sup> are used, the rest are ignored
    * Maximum shift count is 63 (8#77#) for OP 01, 02, 03
    * Maximum shift count is 59 (8#73#) for OP 05, 06, 07
* OP 13: (sic) Skip NI if other computer (on channel 0 or 1) has input buffer active. Execute twice.

| OP | Code        | j | k | Description                                                                     |
| -- | ----------- | - | - | ------------------------------------------------------------------------------- |
| 00 | (fault)     |   |   | Execute instruction at fault interrupt address                                  |
| 01 | RSH Q       | 1 | 1 | Right shift Q by Y, sign fill                                                   |
| 02 | RSH A       | 1 | 1 | Right shift A by Y, sign fill                                                   |
| 03 | RSH AQ      | 1 | 1 | Right Shift AQ by Y, sign fill                                                  |
| 04 | COM A Q AQ  | 3 | 1 | Sense (j)                                                                       |
| 05 | LSH Q       | 1 | 1 | Circular left shift Q by Y                                                      |
| 06 | LSH A       | 1 | 1 | Circular left shift A by Y                                                      |
| 07 | LSH AQ      | 1 | 1 | Circluar left shift AQ By Y                                                     |
| 10 | ENT Q       | 1 | 1 | Load Q by Y                                                                     |
| 11 | ENT A       | 1 | 1 | Load A by Y                                                                     |
| 12 | ENT B       | 1 | 1 | Load B<sup>j</sup> by Y<sub>L</sub>                                             |
| 13 | EX FCT      |   | 4 | j^ /= 0,1: Output EF (Y) on channel C<sup>j^</sub>                              |
|    |             |   |   | j^ == 0,1: Skip NI if channel C<sup>j^</sup> other computer has input active    |
|    |             |   |   | Only k^ == 3 permitted.                                                         |
| 14 | STR Q       | 1 | 2 | (Q) -> Y (k == 0: Complement Q)                                                 |
| 15 | STR A       | 1 | 2 | (A) -> Y (k == 0: Complement A)                                                 |
| 16 | STR B       | 1 | 2 | (B)<sup>j</sup> -> Y                                                            |
| 17 | STR C       |   |   | (C)<sup>j^</sub> -> Y                                                           |
| 20 | ADD A       | 1 | 1 | (A)+Y -> A                                                                      |
| 21 | SUB A       | 1 | 1 | (A)-Y -> A                                                                      |
| 22 | MUL         | 1 | 1 | (Q)*Y -> AQ                                                                     |
| 23 | DIV         | 4 | 1 | (AQ)<sub>i</sub>/Y : Quotient ->Q<sub>f</sub>, Remainder -> A<sub>f</sub>       |
|    | DIV k=7     |   |   | (642B) Square root of \|AQ\| -> Q, Remainder -> A                              |
| 24 | RPL A+Y     | 1 | 3 | (A)+(Y) -> Y and A                                                              |
| 25 | REPL A-Y    | 1 | 3 | (A)-(Y) -> Y and A                                                              |
| 26 | ADD Q       | 2 | 1 | (Q)+Y -> Q                                                                      |
| 27 | SUB Q       | 2 | 1 | (Q)-Y -> Q                                                                      |
| 30 | ENT Y+Q     | 1 | 1 | Y+(Q) -> A                                                                      |
| 31 | ENT Y-Q     | 1 | 1 | 0 -> A. (Q) -> A. (A)-Y -> A. (A)<sup>!</sup> -> A.                             |
| 32 | STR A+Q     | 1 | 2 | (A)+(Q) -> Y, A                                                                 |
| 33 | STR A-Q     | 1 | 2 | (A)-(Q) -> Y, A                                                                 |
| 34 | RPL Y+Q     | 1 | 3 | (Y)+(Q) -> Y, A                                                                 |
| 35 | RPL Y-Q     | 1 | 3 | (Y)-(Q) -> Y, A                                                                 |
| 36 | RPL Y+1     | 1 | 3 | (Y)+1 -> Y, A                                                                   |
| 37 | RPL Y-1     | 1 | 3 | (Y)-1 -> Y, A                                                                   |
| 40 | ENT LP      | 5 | 1 | L[Y*(Q)] -> A                                                                   |
| 41 | ADD LP      | 1 | 1 | L[Y*(Q)]+(A) -> A                                                               |
| 42 | SUB LP      | 1 | 1 | L[Y*(Q)]-(A) -> A                                                               |
| 43 | COM MASK    | 1 | 1 | Compare (A) and L[Y*(Q)]; sense j                                               |
| 44 | RPL LP      | 5 | 3 | L[(Y)*(Q)] -> Y, A                                                              |
| 45 | RPL A+LP    | 1 | 3 | L[Y*(Q)]+(A) -> Y, A                                                            |
| 46 | RPL A-LP    | 1 | 3 | L[Y*(Q)]-(a) -> Y, A                                                            |
| 47 | STR LP      | 1 | 2 | L[(A)*(Q)] -> Y                                                                 |
| 50 | SEL SET     | 1 | 1 | Set bit (A)<sub>n</sub> for bit Y<sub>n</sub> == 1                              |
| 51 | SEL CP      | 1 | 1 | Complement bit (A)<sub>n</sub> for bit Y<sub>n</sub> == 1                       |
| 52 | SEL CL      | 1 | 1 | Clear bit (A)<sub>n</sub> for bit Y<sub>n</sub> == 1                            |
| 53 | SEL SU      | 1 | 1 | Substitute Y<sub>n</sub> -> (A)<sub>n</sub> for (Q)<sub>n</sub> == 1            |
|    |             |   |   | k == 0, 4 should not be used.                                                   |
| 54 | RSEL SET    | 1 | 1 | Set bit (A)<sub>n</sub> for bit Y<sub>n</sub> == 1 -> Y,A                       |
| 55 | RSEL CP     | 1 | 1 | Complement bit (A)<sub>n</sub> for bit Y<sub>n</sub> == 1 -> Y, A               |
| 56 | RSEL CL     | 1 | 1 | Clear bit (A)<sub>n</sub> for bit Y<sub>n</sub> == 1 -> Y, A                    |
| 57 | RSEL SU     | 1 | 1 | Substitute Y<sub>n</sub> -> (A)<sub>n</sub> for (Q)<sub>n</sub> == 1 -> Y, A    |
| 60 | JP          | 7 | 1 | Jump (Arithmetic)                                                               |
| 61 | JP          | 8 | 1 | Jump (Manual)                                                                   |
| 62 | JP IN       |   |   | Jump if channel C<sup>j^</sup> has active input buffer                          |
| 63 | JP OUT      |   |   | Jump if channel C<sup>j^</sup> has active output buffer                         |
| 64 | RJP         | 7 | 1 | Return jump (Arithmetic)                                                        |
| 65 | RJP         | 8 | 1 | Return jump (Manual)                                                            |
| 66 | TERM IN     |   |   | Terminate input buffer on channel C<sup>j^</sup>                                |
| 67 | TERM OUT    |   |   | Terminate output buffer on channel C<sup>j^</sup>                               |
| 70 | RPT         | 6 |   | Execute NI Y times                                                              |
| 71 | BSK         |   | 1 | (B)<sup>j</sup> == Y, 0 ->  B<sup>j</sup>. Skip NI.                             |
|    |             |   |   | (B)<sup>j</sup> /= Y, (B)<sup>j</sup> + 1 -> B<sup>j</sup>. Execute NI.         |
| 72 | BJP         |   | 1 | (B)<sup>j</sup> == 0, Read NI                                                   |
|    |             |   |   | (B)<sup>j</sup> /= 0, Decrement (B)<sup>j</sup>-1 and jump to address Y         |
| 73 | IN          |   | 4 | Enable input buffer on channel C<sup>j^</sup>                                   |
|    |             |   |   | k^ == 2, not permitted. (Base) == 00100.                                        |
| 74 | OUT         |   | 4 | Enable output buffer on channel C<sup>j^</sup>                                  |
|    |             |   |   | k^ == 2, not permitted. (Base) == 00120.                                        |
| 75 | IN MONITOR  |   | 4 | Enable input buffer with monitor on channel C<sup>j^</sup>                      |
|    |             |   |   | k^ == 2, not permitted. (Base) == 00100.                                        |
|    |             |   |   | Monitor interrupt jump address at 00040+j^                                      |
| 76 | OUT MONITOR |   | 4 | Enable output buffer with monitor on channel C<sup>j^</sup>                     |
|    |             |   |   | k^ == 2, not permitted. (Base) == 00120.                                        |
|    |             |   |   | Monitor interrupt jump address at 00060+j^                                      |
| 77 | (fault)     |   |   | Execute instruction at fault interrupt address                                  |

## Repertoire Of Instructions (Detail)

This section describes the detailed operation of each instruction.

Instructions 00 and 77 generate illegal instruction error interrupt and are not listed.

Notes:

* Unless otherwise noted, use J Table 1 for skip conditions

### 01 - Right Shift Q

Shift (Q) to the right Y bit positions with sign extension.

Bit 2<sup>29</sup> before shifting will determine bit 2<sup>29</sup> after each shift.

Y bits 2<sup>05</sup> .. 2<sup>00</sup> are the only bits recognized.
Y bits 2<sup>29</sup> .. 2<sup>06</sup> are ignored.

Example Y=2 Q Positive

| Shift count     | Q<sup>29</sup> .. Q<sup>26</sup> bit values |
| --------------- | ------------------------------------------- |
| (Q)<sub>i</sub> | 0 1 0 1                                     |
| 1               | 0 0 1 0                                     |
| 2               | 0 0 0 1                                     |

Example Y=2 Q Negative

| Shift count     | Q<sup>29</sup> .. Q<sup>26</sup> bit values |
| --------------- | ------------------------------------------- |
| (Q)<sub>i</sub> | 1 1 0 1                                     |
| 1               | 1 1 1 0                                     |
| 2               | 1 1 1 1                                     |

### 02 - Right Shift A

Shift (A) to the right Y bit positions with sign extension.

Bit 2<sup>29</sup> before shifting will determine bit 2<sup>29</sup> after each shift.

Y bits 2<sup>05</sup> .. 2<sup>00</sup> are the only bits recognized.
Y bits 2<sup>29</sup> .. 2<sup>06</sup> are ignored.

Example Y=2 A Positive

| Shift count     | A<sup>29</sup> .. A<sup>26</sup> |
| --------------- | -------------------------------- |
| (A)<sub>i</sub> | 0 1 0 1                          |
| 1               | 0 0 1 0                          |
| 2               | 0 0 0 1                          |

Example Y=2 A Negative

| Shift count     | A<sup>29</sup> .. A<sup>26</sup> |
| --------------- | -------------------------------- |
| (A)<sub>i</sub> | 1 1 0 1                          |
| 1               | 1 1 1 0                          |
| 2               | 1 1 1 1                          |

### 03 - Right Shift AQ

Shift (AQ) to the right Y bit positions with sign extension.

Y bits 2<sup>05</sup> .. 2<sup>00</sup> are the only bits recognized.
Y bits 2<sup>29</sup> .. 2<sup>06</sup> are ignored.

(A) is shifted same as instruction 02.

(A)<sup>00</sup> is shifted into (Q)<sup>29</sup>.

Example Y=2 A Positive (Bit positions not shown == 0)

| Shift count      | A<sup>29</sup> .. A<sup>26</sup> | A<sup>02</sup> .. A<sup>00</sup> | Q<sup>29</sup> .. Q<sup>26</sup> | Q<sup>02</sup> .. Q<sup>00</sup> |
| ---------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| (AQ)<sub>i</sub> | 0 1 0 1                          | 0 0 1 1                          | 1 1 0 0                          | 1 1 0 0                          |
| 1                | 0 0 1 0                          | 1 0 0 1                          | 1 1 1 0                          | 0 1 1 0                          |
| 2                | 0 0 0 1                          | 0 1 0 0                          | 1 1 1 1                          | 0 0 1 1                          |

Example Y=2 A Negative (Bit positions not shown == 0)

| Shift count      | A<sup>29</sup> .. A<sup>26</sup> | A<sup>02</sup> .. A<sup>00</sup> | Q<sup>29</sup> .. Q<sup>26</sup> | Q<sup>02</sup> .. Q<sup>00</sup> |
| ---------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| (AQ)<sub>i</sub> | 1 1 0 1                          | 0 0 1 1                          | 1 1 0 0                          | 1 1 0 0                          |
| 1                | 1 1 1 0                          | 1 0 0 1                          | 1 1 1 0                          | 0 1 1 0                          |
| 2                | 1 1 1 1                          | 0 1 0 1                          | 1 1 1 1                          | 0 0 1 1                          |

### 04 - Compare

Compare the signed value of Y.  See J Table 2 for skip conditions.

### 05 - Left Shift Q

Shift (Q) to the left Y bit positions.

Y bits 2<sup>05</sup> .. 2<sup>00</sup> are the only bits recognized.
Y bits 2<sup>29</sup> .. 2<sup>06</sup> are ignored.

(Q)<sup>29</sup> is shifted into (Q)<sup>00</sup>.

Example Y=3 (Bit positions not shown == 0)

| Shift count      | Q<sup>29</sup> .. Q<sup>26</sup> | Q<sup>02</sup> .. Q<sup>00</sup> |
| ---------------- | -------------------------------- | -------------------------------- |
| (Q)<sub>i</sub>  | 0 1 0 1                          | 0 0 1 1                          |
| 1                | 1 0 1 0                          | 0 1 1 0                          |
| 2                | 0 1 0 0                          | 1 1 0 1                          |
| 3                | 1 0 0 0                          | 1 0 1 0                          |

### 06 - Left Shift A

Shift (A) to the left Y bit positions.

Y bits 2<sup>05</sup> .. 2<sup>00</sup> are the only bits recognized.
Y bits 2<sup>29</sup> .. 2<sup>06</sup> are ignored.

(A)<sup>29</sup> is shifted into (A)<sup>00</sup>.

Example Y=3 (Bit positions not shown == 0)

| Shift count      | A<sup>29</sup> .. A<sup>26</sup> | A<sup>02</sup> .. A<sup>00</sup> |
| ---------------- | -------------------------------- | -------------------------------- |
| (A)<sub>i</sub>  | 0 1 0 1                          | 0 0 1 1                          |
| 1                | 1 0 1 0                          | 0 1 1 0                          |
| 2                | 0 1 0 0                          | 1 1 0 1                          |
| 3                | 1 0 0 0                          | 1 0 1 0                          |

### 07 - Left Shift AQ

Shift (AQ) to the left Y bit positions.

Y bits 2<sup>05</sup> .. 2<sup>00</sup> are the only bits recognized.
Y bits 2<sup>29</sup> .. 2<sup>06</sup> are ignored.

(Q)<sup>29</sup> is shifted into (A)<sup>00</sup>.
(A)<sup>29</sup> is shifted into (Q)<sup>00</sup>.

Example Y=3 (Bit positions not shown == 0)

| Shift count      | A<sup>29</sup> .. A<sup>26</sup> | A<sup>02</sup> .. A<sup>00</sup> | Q<sup>29</sup> .. Q<sup>26</sup> | Q<sup>02</sup> .. Q<sup>00</sup> |
| ---------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| (AQ)<sub>i</sub> | 0 1 0 1                          | 0 0 1 1                          | 1 1 0 0                          | 1 1 0 0                          |
| 1                | 1 0 1 0                          | 0 1 1 1                          | 1 0 0 0                          | 1 0 0 0                          |
| 2                | 0 1 0 0                          | 1 1 1 1                          | 0 0 0 0                          | 0 0 0 1                          |
| 3                | 1 0 0 0                          | 1 1 1 1                          | 0 0 0 0                          | 0 0 1 0

### 10 - Enter Q

Clear Q. Y -> Q.

### 11 - Enter A

Clear A. Y -> A.

### 12 - Enter B<sup>j</sup>

Clear B<sup>j</sup>. Y<sub>L</sub> -> B<sup>j</sup>.

Y<sub>U</sub> bits are ignored.

**j** field designates B register to use and not available for normal skip condition.

### 13 - External Function on C<sup>j^</sup>

See K Table 4 - k^ defines for Y. Only k^ == 3 permitted.

j^ == 0, 1: Check input active designator on connected computer, skip NI if input buffer active.

j^ /= 0, 1: Send EF code Y on channel C<sup>j^</sup>.

Note: No EF lines on intercomputer channels 0, 1

### 14 - Store Q

(Q) -> Y.

**k** == 0: Complement (Q)

### 15 - Store A

(A) -> Y.

**k** == 4: Complement (A)

### 16 - Store B<sup>j</sup>

(B)<sup>j</sup> -> Y<sub>L</sub>. 0 -> Y<sub>U<sub>

**j** field designates B register to use and not available for normal skip condition.

### 17 - Store C<sup>j^</sup>

Data from channel C<sup>j^</sup> -> Y. Send Input Acknowledge on channel C<sup>j^</sup>.

Only k^ == 3 permitted.

### 20 - Add A

(A) + Y -> A.

### 21 - Subtract A

(A) - Y -> A.

### 22 - Multiply

(Q) * Y -> AQ.

If factors are considered integers, product AQ is integer.

k == 7 should not be used.

* (A)<sub>f</sub> /= 0 and j == 4: Double length product with significant bit(s) in A. Recommendation is to re-execute multiply instruction with same operand and test (Q)<sup>29</sup> using j == 2 or 3.

### 23 - Divide

(AQ) / Y, Quotient -> Q, Remainder -> A. (A)<sup>29</sup> == (Q)<sup>29</sup>.

See J Table 4 for Skip NI conditions.

* j == 2: Skip NI on correct answer
* j == 3: Skip NI on divide fault

(642A only)

k == 7 should not be used.

### 23 K=7 = Square Root
(642B only)

k == 7 : Square root

Square root of \|AQ\| -> Q, remainder -> A

\|AQ\| sign is ignored, considered positive number only

### 24 - Replace A + Y

(A) + (Y) -> A. Store (A) -> Y.

### 25 - Replace A - Y

(A) - (Y) -> A. Store (A) -> Y.

### 26 - Add Q

(A) <-> (Q). (A) + Y -> A. (A) <-> (Q).

(A)<sub>f</sub> -- (A)<sub>i</sub>.

### 27 - Subtract Q

(A) <-> (Q). (A) - Y -> A. (A) <-> (Q).

(A)<sub>f</sub> -- (A)<sub>i</sub>.

See J Table 2 for Skip NI conditions.

### 30 - Enter Y + Q

Clear A. (Q) -> A. (A) + Y -> A.

### 31 - Enter Y - Q

Clear A. (Q) -> A. (A) - Y -> A. (A)<sup>!</sup> -> A.

### 32 - Store A + Q

(A) + (Q) -> A. (A) -> Y.

### 33 - Store A - Q

(A) - (Q) -> A. (A) -> Y.

### 34 - Replace Y + Q

Clear A. (Q) -> A. (A) + (Y) -> A. (A) -> Y.

### 35 - Replace Y - Q

Clear A. (Q) -> A. (A) - (Y) -> A. (A)<sup>!</sup> -> Y.

### 36 - Replace Y + 1

Clear A. 1 -> A. (Y) + (A) -> A. (A) -> Y.

### 37 - Replace Y - 1

Clear A. 1 -> A. (A) - (Y) -> A. (A)<sup>!</sup> -> A. (A) -> Y.

### 40 - Enter Logical Product

L[Y*(Q)] -> A.

Use J Table 5 for Skip NI.

### 41 - Add Logical Product

(A) + L[Y*(Q)] -> A.

### 42 - Subtract Logical Product

(A) - L[Y*(Q)] -> A.

### 43 - Compare Masked

(A) - L[Y*(Q)]. (A)<sub>f</sub> == (A)<sub>i</sub>.

See J Table 3 for Skip NI evaluation.

### 44 - Replace Logical Product

L[(Y)*(Q)] -> A. (A) -> Y.

See J Table 5 for Skip NI evaluation.

### 45 - Replace A + Logical Product

(A) + L[(Y)*(Q)]. (A) -> Y.

### 46 - Replace A - Logical Product

(A) - L[(Y)*(Q)]. (A) -> Y.

### 47 - Store Logical Product

L[(A)*(Q)] -> Y.

### 50 - Selective Set

Bit (Y)<sup>nn</sup> == 1: Set (A)<sup>nn</sup> = 1

Bit (Y)<sup>nn</sup> == 0: (A)<sup>nn</sup> = unchanged

Example 1

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 0 0 0 0 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 0 1 0 1 |

Example 2

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 1 0 1 1 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 1 1 1 1 |

### 51 - Selective Complement

Bit (Y)<sup>nn</sup> == 1: Set (A)<sup>nn</sup> = ((A)<sup>nn</sup>)<sup>!</sup>

Bit (Y)<sup>nn</sup> == 0: (A)<sup>nn</sup> = unchanged

Example 1

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 0 0 0 0 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 0 1 0 1 |

Example 2

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 1 1 0 1 1 |
| Y              | 0 1 0 1 0 |
| A<sub>f</sub>  | 1 0 0 0 1 |

### 52 - Selective Clear
Bit (Y)<sup>nn</sup> == 1: Set (A)<sup>nn</sup> = 1

Bit (Y)<sup>nn</sup> == 0: (A)<sup>nn</sup> = unchanged

Example 1

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 0 0 0 0 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 0 1 0 1 |

Example 2

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 1 0 1 1 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 1 1 1 1 |

Bit (Y)<sup>nn</sup> == 1: Set (A)<sup>nn</sup> = 1

Bit (Y)<sup>nn</sup> == 0: (A)<sup>nn</sup> = unchanged

Example 1

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 0 0 0 0 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 0 1 0 1 |

Example 2

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 1 0 1 1 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 1 1 1 1 1 |

Example 1

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 1 1 1 1 1 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 0 1 0 1 0 |

Example 2

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 0 1 0 1 1 |
| Y              | 1 0 1 0 1 |
| A<sub>f</sub>  | 0 1 0 1 0 |

### 53 - Selective Substitute

Bit (Q)<sup>nn</sup> == 1: (Y)<sup>nn</sup> -> (A)<sup>nn</sup>.

Bit (Q)<sup>nn</sup> == 0: (A)<sup>nn</sup> unchanged.

k == 0, 4 should not be used.

Example 1

|                | Bits      |
| -------------- | --------- |
| A<sub>i</sub>  | 1 0 1 1 1 |
| Q              | 1 0 1 0 1 |
| Y              | 1 1 0 0 1 |
| A<sub>f</sub>  | 1 0 0 1 1 |

### 54 - Replace Selective Set

Set bit (A)<sup>nn</sup> where (Y)<sup>nn</sup> == 1. (A) -> Y.

See 50 - Selective Set for bit examples.

### 55 - Replace Selective Complement

((A)<sup>nn</sup>)<sup>!</sup> -> (A)<sup>nn</sup> where (Y)<sup>nn</sup> == 1. (A) -> Y.

See 51 = Selective Complement for bit examples.

### 56 - Replace Selective Clear

Bit (Y)<sup>nn</sup> == 1: Set (A)<sup>nn</sup> = 1.

Bit (Y)<sup>nn</sup> == 0: (A)<sup>nn</sup> = unchanged

(A) -> Y.

See 52 - Selective Clear for bit examples.

### 57 - Replace Selective Substitute

Bit (Q)<sup>nn</sup> == 1: (Y)<sup>nn</sup> -> (A)<sup>nn</sup>.

Bit (Q)<sup>nn</sup> == 0: (A)<sup>nn</sup> unchanged.

(A) -> Y.

See 53 - Selective Substitute for bit examples.

### 60 - Jump (Arithmetic)

See J Table 7 for jump conditions.

Jump condition satisfied: 0 -> P. Y -> P.

Jump condition not satisfied: Execute NI.

### 61 - Jump (Manual)

See J Table 8 for jump conditions.

Jump condition satisfied: 0 -> P. Y -> P.

Jump condition not satisfied: Execute NI.

### 62 - Jump C<sup>j^</sup> Active Input Buffer

See K Table 4 - k^ field defines for Y source.

Channel C<sup>j^</sup> input buffer active: 0 -> P. Y -> P.

Channel C<sup>j^</sup> input buffer not active: Execute NI.


### 63 - Jump C<sup>j^</sup> Active Output Buffer

See K Table 4 - k^ field defines for Y source.

Channel C<sup>j^</sup> output buffer active: 0 -> P. Y -> P.

Channel C<sup>j^</sup> output buffer not active: Execute NI.

### 64 - Return Jump (Arithmetic)

See J Table 7 for jump conditions.

Normal operation:

* Jump condition satisfied: 1 -> p. (P)+(p) -> Y<sub>L</sub>. Y<sub>L</sub>+1 -> P.
* Jump condition not satisfied: Execute NI.

Interrupt operation:

* Set Interrupt Disable
* (P)+<sub>p</sup> -> Y<sub>L</sub>
* Y+1 -> P

### 65 - Return Jump (Manual)

See J Table 8 for jump conditions.

Note: Should use OP 64 - Return Jump (Arithmetic) for interrupts.

Normal operation:

* Jump condition satisfied: 1 -> p. (P)+(p) -> Y<sub>L</sub>. Y<sub>L</sub>+1 -> P.
* Jump condition not satisfied: Execute NI.

Interrupt operation:

* Set Interrupt Disable
* (P)+<sub>p</sup> -> Y<sub>L</sub>
* Y+1 -> P

### 66 - Terminate C<sup>j^</sup> Input Buffer

Terminate input buffer on channel C<sup>j^</sup>. No monitor interrupt will be generated.

Instruction fields **k^**, **b**, and **y** are not used.

### 67 - Terminate C<sup>j^</sup> Output Buffer

Terminate output buffer on channel C<sup>j^</sup>. No monitor interrupt will be generated.

Instruction fields **k^**, **b**, and **y** are not used.

### 70 - Repeat

Notes:

B<sup>7</sup> contains number of executions remaining during repeat mode.

See NI J Table for NI skip conditions.

Program Example

| Relative address | Designator | Description           |
| ---------------- | ---------- | --------------------- |
| 0                | RI         | Repeat Instruction    |
| 1                | NI         | Instruction to repeat |
| 2                | SNI        | Instruction after NI  |
| 3                | PC         | Continue Program      |

Sequence of operation:

Execute RI: 0 -> B<sup>7</sup>. Y<sub>L</sub> -> B<sup>7</sup>.

B<sup>7</sup> == 0: Continue program at address 2

B<sup>7</sup> /= 0: (**j**) -> r. See J Table 6 for r interpretation. Execute following steps:

* (B)<sup>7</sup> == 0: Continue program at address 2
* (B)<sup>7</sup> /= 0: (B)<sup>7</sup>-1 -> B<sup>7</sup>. Execute NI at address 1.
* NI skip condition satisfied: Continue program at address 3
* NI skip condition not satisfied: Repeat above steps.

### 71 - B Skip On B<sup>j</sup>

(B)<sup>j</sup> == Y<sub>L</sub>: 0 -> B<sup>j</sup>. Skip NI.

(B)<sup>j</sup> /= Y<sub>L</sub>: (B)<sup>j</sup> + 1 -> B<sup>j</sup>. Execute NI.

### 72 - B Jump on B<sup>j</sup>

(B)<sup>j</sup> == 0: Execute NI.

(B)<sup>j</sup> /= 0: (B)<sup>j</sup> - 1 -> B<sup>j</sup>. Y<sub>L</sub> -> P.

### 73 - Input Buffer on C<sup>j^</sup>

See K Table 4 - k^ Field Defines for **k^** interpretation. k^ == 2 not permitted.

Establish input buffer on channel C<sup>j^</sup>. (Base) == 00100.

### 74 - Output Buffer on C<sup>j^</sup>

See K Table 4 - k^ Field Defines for **k^** interpretation. k^ == 2 not permitted.

Establish output buffer on channel C<sup>j^</sup>. (Base) == 00120.

### 75 - Input Buffer on C<sup>j^</sup> With Monitor

See K Table 4 - k^ Field Defines for **k^** interpretation. k^ == 2 not permitted.

Establish input buffer on channel C<sup>j^</sup>.
(Base) == 00100.
(Monitor Base) == 00040.

On buffer terminate, execute instruction at (Monitor Base)+C<sup>j^</sup>.

### 76 - Output Buffer on C<sup>j^</sup> With Monitor

See K Table 4 - k^ Field Defines for **k^** interpretation. k^ == 2 not permitted.

Establish output buffer on channel C<sup>j^</sup>.
(Base) == 00120.
(Monitor Base) == 00060.

On buffer terminate, execute instruction at (Monitor Base)+C<sup>j^</sup>.
