
# AN/USQ-20 Basic Description

The AN/USQ-20 NTDS system has a basic design that all systems incorporate. During the life of the USQ-20 project, there were upgrades that improved hardware, but all upgrades followed the basic USQ-20 design.

This document covers the basic design of the USQ-20 system.

USQ-20 computer designator is normally associtated with the CP642A computer.

## Definitions

### General

    Industry standards definitions apply
    ~  = (tilde) approximately

Number base is designated using Ada standard.

    2#..# interpreted in base 2 (binary)
    8#..# interpreted in base 8 (octal)
    10#..# interpreted in base 10 (decimal)
    16#..# interpreted in base 16 (hexadecimal)

    All other numbers are interpreted as decimal unless otherwise noted.

### Unit Of Measure Modifiers

Industry standard definitions apply. This section is for quick reference only.

| Modifier | Designator | Pronounced | Description               |
| -------- | ---------- | ---------- | ------------------------- |
| G        | Uppercase G  | Giga     | Multiply by 1_000_000_000 |
| M        | Uppercase M  | Mega     | Multiply by 1_000_000     |
| K        | Uppercase K  | Kilo     | Multiply by 1_000         |
| m        | Lowercase M  | Milli    | Multiply by 0.001         |
| u        | Lowercase U  | Micro    | Multiply by 0.000_001     |
| n        | Lowercase N  | Nano     | Multiply by 0.000_000_001 |

Normal usage of Hertz modifier is in units/operations per second

    1Hz  = 1.0 Hertz
    1KHz = 1_000.0 Hertz
    1MHz = 1_000_000.0 Hertz
    1GHz = 1_000_000_000.0 Hertz

Normal usage of time modifier is unit/modifier where:

    1S   = 1.0 second (1S / 1Hz)
    1mS  = 0.001 second (1S / 1KHz)
    1uS  = 0.000_001 second (1S / 1MHz)
    1nS  = 0.000_000_001 second (1S / 1GHz)

## Symbols

Following symbols are used for interpreting registers and instructions

| Symbol           | Description               | Notes                                                    |
| ---------------- | ------------------------- | -------------------------------------------------------- |
| ( )              | Contents of               |                                                          |
|                  | Content of register       | (A) Content of register A                                |
|                  | Content of register       | (B<sub>n</sub> Content of register B<sub>n</sub>         |
|                  | Content of memory         | (Y) Content of memory location Y                         |
| a                | Source or destination     | Lowercase a                                              |
| A, Q, B, etc     | Register                  | Capital letter. Register A, Q, B, etc.                   |
| a<sub>i</sub>    | Initial value of a        | Subscript Lowercase i                                    |
| a<sub>f</sub>    | Final value of a          | Subscript Lowercase f                                    |
| a<sub>l</sub>    | Lower group               | Subscript lowercase l = Bits 14 .. 00                    |
| a<sub>n</sub>    | Bit position              | Subscript Lowercase n                                    |
| a<sub>u</sub>    | Upper group               | Subscript lowercase u = Bits 29 .. 15                    |
| B<sub>n</sub>    | B register                | n = selected B register                                  |
| C<sub>n</sub>    | Channel number            | n = I/O channel number                                   |
| **f**            | Function code             | Bold lowercase f = Bits 29 .. 24                         |
| **j**            | Jump, skip, stop          | Bold lowercase j = Bits 23 .. 21 (Normal instruction)    |
|                  | condition determination   |                                                          |
| **j^**           | Channel designator        | Bold lowercase j caret = Bits 23 .. 20 (I/O instruction) |
|                  |                           | Documentation may show caret '^' above letter            |
| **k**            | Instruction modifier      | Bold lowercase k = Bits 20 .. 18                         |
| **k^**           | Channel modifier          | Bold lowercase k caret = Bits 19 .. 18                   |
|                  |                           | Documentation may show caret '^' above letter            |
| **b**            | Index register            | Bold lowercase b = Bits 17 .. 15                         |
| **y**            | Operand                   | Bold lowercase y = Bits 14 .. 00                         |
| y                | Constant or operand       | Lowercase y                                              |
|                  | As source                 | Where to find value                                      |
|                  | As destination            | Where to send/save value                                 |
|                  | As constant/value         | Direct value to use                                      |
| Y                | Address designator        | Uppercase Y. Memory location.                            |

## Hardware design

* Word size is 30 bits (2**30)
* Internal 8#100_000# (32_768) word memory
    * 0 to 8#77_777#
    * Some memory reserved - see memory table for specific CPU
* Asynchronous I/O (initiated by CPU)
    * I/O data is size of word
    * Buffered I/O (with/without monitor interrupt)

## Timing Considerations

* ~1MHz clock (680ns-1uS resolution)
    * 4-phases ~170ns each
    * Phases 1, 2, 3, 4
* Main timing broken into several block
* Sequencers ensure proper cycle timing
* Initial sequencers
    * Initial
        * A<sub>i</sub>
        * I/O<sub>i</sub>
        * INT<sub>i</sub>
        * EXTINT<sub>i</sub>
    * Final
        * A<sub>f</sub>
        * B<sub>f</sub>
        * D<sub>f</sub>
        * I/O<sub>f</sub>
        * INT<sub>f</sub>
* Main Timing cycles
    * Each timing chain takes ~1 clock cycle
        * Tx1 .. Tx4 (not including MTS)
    * 6 timing chains
        * 1-2-3-4-5, MTS
    * Each standard chain consists of 4 f/f in series
        * T11-T12-T13-T14-T21-T22-T23-T24-...-T51-T52-T53-T54
            * Tx1 clocks on phase 2
            * Tx2 clocks on phase 3
            * Tx3 clocks on phase 4
            * Tx4 clocks on phase 1
    * Main Timing Special conisists of 8 f/f with selected enables
        * T23-T24-T34-T43-T52 MTS
* Sequence Timing maps sequencers + main timing cycles to enables
    * 1
        * Abort timing check is A<sub>f</sub> and T53
    * 2-5
* RTC
    * 1KHz internal clock (1mS resolution)
        * See specific CPU for exact resolution
    * External RTC clock connection available
* Memory cycle time 4-8uS
    * See specific CPU for exact timing

### I/O Data Transfer Rate

Nominal data transfer rate is basic design. NTDS Fast/Slow are upgraded design.

* Nominal data transfer rates of ~30KHz
    * Input data transfer maximum rate ~25MHz (40uS minimum resolution)
    * Output data transfer maximum rate ~42.7MHz (23.4uS minimum resolution)
    * External function data transfer maximum rate ~ 35.2MHz (28.4uS minimum resolution)
    * Input/External function interrupt data transfer maximum rate ~35.2MHz (28.4uS minimum resolution)

* NTDS Slow nominal data transfer rates of ~40KHz
    * Input data transfer maximum rate ~32KHz (31.3uS minimum resolution)
    * External interrupt data transfer maximum rate ~33KHz (30.3uS minimum resolution)
    * Output data/external function data transfer maximum rate ~58KHz (17.0uS minimum resolution)
    * External function with force data transfer maximum rate ~42KHz (24.0uS minimum resolution)

* NTDS Fast nominal data transfer rates of ~125KHz
    * Input data transfer maximum rate ~80KHz (12.4uS minimum resolution)
    * External interrupt data transfer maximum rate ~101KHz (9.9uS minimum resolution)
    * Output data/external function data transfer maximum rate ~76KHz (13.2uS minimum resolution)
    * External function with force data transfer maximum rate ~131KHz (7.6uS minimum resolution)

Notes:

1. Rates are per channel
1. Fastest data rate(s) for all channels combined are determined by memory timing

## Initial design

* 30-bit word length with 15-bit half-word access modifier
* Internal stored program using 32,768 word capacity memory
    * Some memory is reserved - see table below
* 62 instruction repertoir with conditional program branch and skip NI modifier
* Programmed checking of data parity
* Parallel, one's complement, subtractive arithmetic modulus 2^30 - 1
* Single address instructions
    * Provision for address modification via index (B) registers
    * Provision for half-word (upper 15 bits/lower 15 bits) access
    * Provision for skipping next instruction modifier
* 14 Input data channels (buffered with/without monitor)
* 14 Output data channels (buffered with/without monitor)
* Normal execution sequences:
    * A - Read next instruction
    * B - Read operand/preliminary arithmetic operations
    * C - Arithmetic Manipulation
    * D - Store operand (if needed)
* Input/Output Execution sequences (May be initiated between any normal execution sequence):
    * E - I/O Read buffer address
    * F - I/O Transfer buffer word
    * G - I/O Store buffer address
* Repeat next instruction mode
* Hardwired memory for initial loading or handling fault condition

## CP-642A

* 8uS memory cycle time (4uS read/4uS write/restore)
* 12 computer-to-peripheral (normal) I/O channels
* 2 computer-to-computer I/O channels
* External function output are synchronous with program
* All other I/O is asynchronous

## CP-642B

* Master clock ~1MHz
    * ~680nS cycle time
    * 4-phase (A, B, C, D)
    * 3-phase memory cycle (E, F, G)
    * 130nS per phase +/- 15nS
* RTC clock 
    * Internal 1.024KHz +/- 2Hz
    * External clock pulse selectable
* 32_762 30-bit core memory (destructive read)
    * 4uS memory cycle time
    * Address range 8#00_100# to 8#00_177# access control memory
        * 64 word control memory (NDRO magnetic thin-film)
        * No instructions may be executed from this memory
        * 667nS cycle time
        * B-index registers
        * I/O control words
        * RTC
    * Addressed via S0 register
    * Data accessed via Z0 register
* 64 word bootstrap UNIFLUXOR memory (NDRO)
    * 667nS read cycle time
    * 2 groups of 32 word programs selectable on control panel
* 14 computer-to-peripheral I/O channels
* 2 computer-to-computer I/O channels
* I/O channels can be assigned in groups of 4 for specific interface types
    * Type I interface for computer-peripheral only
    * Type II interface for either computer-peripheral or computer-computer
    * NTDS Slow
    * NTDS Fast
    * Other types as available
    * All I/O is asynchronous
    * External function output may be synchronous as needed for compatibility

# Memory Interrupt Priority

## CP-642A:

1.  Advance RTC
1.  External Interrupt
1.  Internal Output Monitor Interrupt
1.  Internal Input Monitor Interrupt
1.  Output Request on normal channel
1.  Input Request
1.  Output Request on an intercomputer channel

Notes:

1. For simultaneous requests on normal channels, priority is given in descending order of channel number
1. For simultaneous requests on intercomputer channels, priority alternates between the two

## CP-642B:

1. Advance RTC
1. External Function
1. Output Request
1. Input Request
1. External Interrupt
1. External Function Monitor Interrupts
1. Output Monitor Interrupt
1. Input Monitor Interrupt

Notes:

1. For simultaneous requests on channels, priority is given in descending order of channel number

# Register assignments

The below tables indicate the registers used in the USQ-20 family of computers

## Main Registers

| Designator | Bits | Function                                       |
| ---------- | ---- | ---------------------------------------------- |
| A          | 30   | Accumulator (main)                             |
| B          | 15   | Index Register                                 |
| D          | 30   | Accumulator (second rank)                      |
| K          | 6    | Shift Counter                                  |
| P          | 15   | (Uppercase P) Program Address Register         |
| p          | 2    | (Lowercase P) P-Designator Register            |
|            |      | Increments +0, +1, +2 to P-Register            |
| Q          | 30   | Quotient Register                              |
| S          | 15   | Storage Address Register                       |
| U          | 30   | Program Control Register (current instruction) |
| X          | 30   | Arithmetic Exchange Register                   |
| Z          | 30   | Memory Buffer Register                         |
| Zu         | 15   | Z-register upper 15 bits                       |
| Zl         | 15   | Z-register lower 15 bits                       |

## CP-642A

| Designator | Bits | Function                                       |
| ---------- | ---- | ---------------------------------------------- |
| C0         | 30   | Output Register (normal I/O)                   |
| C1         | 30   | Output Register (intercomputer I/O)            |
| R          | 15   | B-Index Input Register                         |
|            |      | B-Index Adder Register                         |
| R*         | 15   | B-Index Output Register                        |

## CP-642B

| Designator | Bits | Function                                       |
| ---------- | ---- | ---------------------------------------------- |
| C1         | 30   | I/O Buffer Register channel group 1            |
| C2         | 30   | I/O Buffer Register channel group 2            |
| C3         | 30   | I/O Buffer Register channel group 3            |
| C4         | 30   | I/O Buffer Register channel group 4            |
| K1,K2,K3   | 6    | Shift counter registers                        |
| R          | 15   | B-Index Adder Register                         |
| S0         | 7    | Control Memory Address Register                |
| W          | 30   | Arithmetic Exchange Register                   |
| Z0         | 15   | Control/Unifloxor memory data register         |

# Word Formats

The below tables indicate which bits are used for each type of word

## Word format

| word |
| ---- |
| 29-0 |

## Half-Word Format

| upper | lower |
| ----- | ----- |
| 29-16 | 15-00 |

## Normal instruction word

**f** /= [ 13, 17, 62, 63, 66, 67, 73-76 ] normal instruction format

| **f** | **j** | **k** | **b** | **y** |
| ----- | ----- | ----- | ----- | ----- |
| 29-24 | 23-21 | 20-18 | 17-15 | 14-00 |

## I/O instruction word

f == [ 13, 17, 62, 63, 66, 67, 73-76 ] I/O instruction format

| **f** | **j^** | **k^** | **b** | **y** |
| ----- | ------ | ------ | ----- | ----- |
| 29-24 | 23-20  | 19-18  | 17-15 | 14-00 |

# Memory Assignment

15-bit addressing allows 32,768 words of address space. However, some addresses are pre-assigned usage for normal computer operation.

Notes:

1. Unless otherwise specified, values given are octal
1. Not assigned addresses may be used by program without side effects

## CP-642A Special Memory Assignment

| Address     | Storage Function                                               |
| ----------- | -------------------------------------------------------------- |
| 00000       | Fault entrance address                                         |
| 00001-17    | Not assigned *                                                 |
| 00020-35    | External interrupt entrance address for normal channels 0-15   |
| 00036       | RTC                                                            |
| 00037       | Not assigned                                                   |
| 00040-55    | Internal interrupt entrance address for input channels 0-15    |
| 00056-57    | Not assigned                                                   |
| 00060-75    | Internal interrupt entrance address for output channels 0-15   |
| 00076-77    | Not assigned                                                   |
| 00100-15    | Input buffer control address for input channels 0-15           |
| 00116-17    | Not assigned                                                   |
| 00120-35    | Output buffer control address for output channels 0-15         |
| 00136-77777 | Not assigned                                                   |

Notes:

1. 00001-017 Hardwired bootstrap if bootstrap mode, othwerwise core memory
1. 00001-017 are not assigned during normal operation, may be masked by wired memory during
bootstrap or recovery mode.

## CP-642B Special Memory Assignment

| Address     | Type      | Storage Function                             |
| ----------- | --------- | -------------------------------------------- |
| 00000       | Core      | Fault entrance register                      |
| 00001-017   | Core      | Not assigned                                 |
| 00020-037   | Core      | External interrupt entrance                  |
| 00040-057   | Core      | Input Monitor interrupt entrance             |
| 00060-077   | Core      | Output Monitor interrupt entrance            |
| 00100-117   | Thin Film | Input Buffer control registers               |
| 00120-137   | Thin Film | Output Buffer control registers              |
| 00140-157   | Thin Film | External Function buffer control registers   |
| 00160       | Thin Film | RTC                                          |
| 00161-167   | Thin Film | B-Registers                                  |
| 00170-177   | Thin Film | Not assigned                                 |
| 00200-477   | Core      | Not assigned                                 |
| 00500-537   | ?         | ?                                            |
| 00540-577   | Unifluxor | NDRO bootstrap                               |
| 00600-617   | Core      | Intercomputer timeout interrupt entrance     |
| 00620-77777 | Core      | Unassigned                                   |

Notes:

1. 00540-577 Bootstrap program I or II depending on boot select switch (for programming purpose only)
1. 00500-537 Assumes bootstrap program I address remapped to 00540-577 for program access only


## MU-602 Extended Core Memory Unit (ECMU) (Control Data Corp - CDC)

* (ECMU memory mods not available at this time)
* External Core Memory Unit (ECMU) adds additional 0-8#1_000_000# (262_144) word memory
* 8#100_000# (cpu internal) + 0 to 8#777_777# (ECMU external)
* Separate equipment similar in size to computer
* ECMU shared with up to 4 computers
* Total addressable memory available per computer 8#1_100_000# (294_912) word memory
* ECMU accessed via extra ECMU mod register(s) in memory control unit
