
# Basic Sequence Flow

The computer is based on timing sequences. They start witht the 4-phase master clock.
Each sequence uses the phases to define specific control signals that are generated.

* Phases labelled A-D are used for program control circuits
* Phases labelled E-G are used for I/O control circuits
    * Same pulses as A-B-D, but used to differentiate between control and I/O circuit timings

* Master Clock
    * ~ 680nS cycle time split into 4 phases
    * ~ 130nS per phase
        * Ø1-Ø2-Ø3-Ø4
    * Clock phases are toggled via
        * Free-running clock ~150nS pulse time
        * Adjustable low-speed clock ~0.5mS-0.005mS pulse time
            * Only in phase-step operations
        * Start/Step toggle switch
    * Sets main/memory timing chain cycle timing

* Main timing is split into 6 sections of 4 steps
    * 1.x-6.x
    * x.1 toggled on Ø2
    * x.2 toggled on Ø3
    * x.3 toggled on Ø4
    * x.4 toggled on Ø1

* Instruction execution is split into 4 main sequences
    * A(E)
        * Used for read instruction
        * Multiple A sequences
            * A<sub>i</sub>
            * A
            * A<sub>f</sub>
    * B(F)
        * Used for read operand and initial updates
        * Multiple B sequences
            * B<sub>i</sub>
            * B
            * B<sub>f</sub>
    * C
        * Independent of A, B, D sequences
        * Used for arithmetic operations
        * Add/subtract/multiply/divide/shift
    * D(G)
        * Used for store operand
        * Multiple D sequences
            * D<sub>i</sub>
            * D
            * D<sub>f</sub>
