# Communications

Since the USQ-20 Emulator will be based on commodity computers, the design considerations include
using standard network protocols to simulate hardware connections within the USQ-20 environment.

With that in mind, this section details the communications between the computers to emulate the
actual hardware connections.

## Overview

Basic design of the environment it based on the following principles:

    * Master physical computer for controlling simulation setup
    * Distributed physical computer(s) running simulation software
    * LAN setup for control/status communications between physical computers
    * LAN setup for communications between USQ-20 devices

## Basic Emulator Configuration

LAN design is based on the IP protocol suite:

    * Control communication will use the TCP protocol
    * USQ-20 device communication will use the UDP protocol
    * Default port for control communication is port 50_000
    * Default base port for first USQ-20 device is control port + 1
    * USQ-20 device port assignment will be (base port) + (channel number)

# USQ-20 Setup

## Common Computer Connections

| Computer Channel | Remote Device | Remote Channel | Notes        |
| :--------------: | :------------ | :------------: | :----------- |
| N/A              | ECMU          |                | (1)          |
| 00               | Computer B    | 00             |              |
| 01               | Computer C    | 00             |              |
| 02               | KCMX          | 00             | Fire Control / Ship Sensors |
| 03               |               |                |              |
| 04               | UGC-13        | 00             | Link-14      |
| 05               | Dislpay       | 00             | (3)(4)       |
| 06               | Display       | 01             |              |
| 07               |               |                |              |
| 10               |               |                |              |
| 11               | Terminal      | 00             | Link-11      |
| 12               | BVP           | 00             | IFF          |
| 13               | MTU           | 00             | (2)          |
| 14               |               |                |              |
| 15               |               |                |              |
| 16               |               |                |              |
| 17               |               |                |              |

1. External Core Memory Unit (ECMU) is a separate memory unit shared between the computers.
2. Normally, MTU is connected to only 1 computer at a time for specific tasks, such as initial program loading, saved operational mapping setup (i.e. commercial air traffic routes, training scenario, etc.), or data capture.
3. SYA-4 Display system is normally split so 2 computers share display duties. Emergency configuration allows 1 computer to control all dislpays.
4. SYA-4 consoles use a common Refresh Memory Unit (RMU) to maintain static symbology on consoles. UYA-4 consoles have individual built-in RMU.

## System Connections

The below table shows the basic setup for a USQ-20 system on a guided missile cruiser (CG):

| Device     | Connection To | Remote Port    | Notes        |
| :--------- | :-----------: | :------------: | :----------- |
| Computer A | Computer B    | 00             | Master       |
|            | Computer C    | 01             |              |
|            | KCMX          | 01             |              |
|            | MTU           | 00             |              |
| Computer B | Computer A    | 00             | Slave 1      |
|            | Computer C    | 01             |              |
|            | Display       | 00             |              |
|            | Terminal      | 00             |              |
| Computer C | Computer A    | 01             | Slave 2      |
|            | Computer B    | 01             |              |
|            | Display       | 01             |              |
|            | BVP           | 00             |              |
|            | UGC-13        | 00             |              |

