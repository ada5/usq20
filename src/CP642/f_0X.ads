package USQ20.f_0X is

    -- Package f_OX are the procedures that execute f-codes 00-07.

    procedure f00;  -- Illegal    Generates fault interrupt
    procedure f01;  -- RSH Q      Right shift Q sign fill
    procedure f02;  -- RSH A      Right shift A sign fill
    procedure f03;  -- RSH AQ     Right shift AQ sign fill
    procedure f04;  -- COM AQ     Compare AQ
    procedure f05;  -- LSH Q      Circular left shift Q
    procedure f06;  -- LSH A      Circular left shift A
    procedure f07;  -- LSH AQ     Circular left shift AQ

end USQ20.f_0X
