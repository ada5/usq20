package USQ20.CP642 is

    -- The CP642 package defines the basic NTDS computer based on the original
    -- NTDS programming guide.
    -- USQ20.CP642A extends USQ20.CP642 with A updates
    -- USQ20.CP642B extends USQ20.CP642 with B updates

end USQ20.CP642;
