
package body USQ20.f_0X is

    procedure f00 is
    -- Illegal    Generates fault interrupt
    begin
        null;
    end

    procedure f01 is
        -- RSH Q      Right shift Q sign fill.
        --            Bit 29 is sign bit.
        --            y <= 77
    begin
        null;
    end

    procedure f02 is
        -- RSH A      Right shift A sign fill
        --            Bit 29 is sign bit.
        --            y <= 77
    begin
        null;
    end

    procedure f03 is
        -- RSH AQ     Right shift AQ sign fill
        --            A bit 29 is sign bit.
        --            A bit 00 shifts into Q bit 29
        --            y <= 77
    begin
        null;
    end

    procedure f04 is
        -- COM A/Q/AQ  Compare A, Q, or AQ with sign-extended Y
        --             If Y == y, sign is y bit 14
        --             j=0 : Do not skip NI
        --             j=1 : Skip NI
        --             j=2 : Skip NI (Q) >= y
        --             j=3 : Skip NI (Q) < y
        --             j=4 : Skip NI (Q) >= y > (A)
        --             j=5 : Skip NI (A) >= y > (Q)
        --             j=6 : Skip NI (A) >= y
        --             j=7 : Skip NI (A) < y
    begin
        null;
    end

    procedure f05 is
        -- LSH Q      Circular left shift Q
        --            Bit 29 shifts into bit 00
        --            y <= 073
    begin
        null;
    end

    procedure f06 is
        -- LSH A      Circular left shift A
        --            Bit 29 shifts into A bit 00
        --            y <= 073
    begin
        null;
    end

    procedure f07 is
        -- LSH AQ     Circular left shift AQ
        --            Q bit 29 shifts into A bit 00
        --            A bit 29 shifts into Q bit 00
        --            y <= 073
    begin
        null;
    end


end USQ20.F_0x