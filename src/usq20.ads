with System;

package USQ20 is

end USQ20;

-- package USQ20.CP642 is separate (NTDS General 30-bit GP computer)
-- package USQ20.CP642A is separate (Univac 1206 30-bit GP computer)
-- package USQ20.CP642B is separate (Univac 1206 30-bit GP computer)
-- package USQ20.MU602 is separate (External Core Memory Unit for CP642)
-- package USQ20.CP789 is separate (Uniac 1218/1219 18-bit GP computer)
-- package USQ20.KCMX is separate (AD/DA converter, status/control bits for fire control)
-- package USQ20.UGC13 is separate (5-level modified TTY)
-- package USQ20.RD231 is separate (5-level paper tape punch/reader - usually part of UGC13))
-- package USQ20.RD243 is separate (magnetic tape storage)
-- package USQ20.RD358 is separate (magnetic tape storage)
