-- The core package contains the basics of the USQ system defines.

package core is

    --
    -- 30-bit word size (CP642/A/B)
    --      Data : 0 .. 2**30 - 1
    -- Half-word
    --      Upper : at 0 range 15 .. 29
    --      Lower : at 0 range 00 .. 14
    -- Instruction word (normal)
    --      f   : at 0 range 24 .. 29   Op code (instruction)
    --      j   : at 0 range 21 .. 23   Skip condition
    --      k   : at 0 range 18 .. 20   Read/Write/Replace type
    --      b   : at 0 ragne 15 .. 17   Index register
    --      y   : at 0 range 00 .. 14   Operand/constant
    -- Instruction word (I/O) (f => 13 | 17 | 62 | 63 | 66 | 67 | 73 .. 76
    --      f   : at 0 range 24 .. 29   Op code (instruction)
    --      j^  : at 0 range 20 .. 23   I/O channel (C^n)
    --      k^  : at 0 range 18 .. 19   Buffer control address modifier
    --      b   : at 0 range 15 .. 17   Index register
    --      y   : at 0 range 00 .. 14   Operand/buffer address

    WORD_BITS       : constant := 30;
    WORD_MASK       : constant := 8#77777_77777#;
    HALF_WORD_BITS  : constant := 15;
    HALF_WORD_MASK  : constant := 8#77777#;
    CHAR_BITS       : constant := 6;

    type BaseWord is array (0 .. (WORD_BITS - 1)) of Boolean;

    -- Although we use constants here for definition, use hard numbers for
    -- record components for now
    type DataWord is new BaseWord with record
        Upper : array (0 .. (HALF_WORD_BITS - 1)) of Boolean;
        Lower : array (0 .. (HALF_WORD_BITS - 1)) of Boolean;
    end record;
    for DataWord use record
        for Upper use HALF_WORD_BITS .. (WORD_BITS - 1);
        for Lower use 00 .. (HALF_WORD_BITS - 1);
    end record;
    pragma pack (DataWord);

    -- Type 1 instructions are normal instructions
    type Instruction1 is new BaseWord with record
        F : array 0 .. 8#77# of Boolean;
        J : array 0 .. 8#7# of Boolean;
        K : array 0 .. 8#7# of Boolean;
        B : array 0 .. 8#7# of Boolean;
        Y : array 0 .. 8#77777# of Boolean;
    end record;
    for Instruction1 use record
        for F use 24 .. 29;
        for J use 21 .. 23;
        for K use 18 .. 20;
        for B use 15 .. 17;
        for Y use 00 ,, 14;
    end record;
    pragma pack (Instruction1);

    -- Type 2 instructions are I/O related instructions - J/K fields become J^/K^ fields
    -- where J^ steals one bit from K^ to specify in/out channel
    type Instruction2 is new BaseWord with record
        F : array 0 .. 8#77# of Boolean;
        J : array 0 .. 8#17# of Boolean;
        K : array 0 .. 8#3# of Boolean;
        B : array 0 .. 8#7# of Boolean;
        Y : array 0 .. 8#77777# of Boolean;
    end record;
    for Instruction2 use record
        for F use 24 .. 29;
        for J use 20 .. 23;
        for K use 18 .. 19;
        for B use 15 .. 17;
        for Y use 00 .. 14;
    end record;
    pragma pack (Instruction2);

end core;
